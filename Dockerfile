FROM node:10.15.3
RUN mkdir -p /var/www/test-ci/
COPY . /var/www/test-ci/
LABEL maintainer="1986tianxie@sina.com"
WORKDIR /var/www/test-ci/
EXPOSE 8080
ENTRYPOINT ["node"]
CMD ["index.js"]
